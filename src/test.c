#include <stdlib.h>
#include <stdio.h>

int main() {
    // char *a = "aaa";
    char *a = "10";
    char *tail;
    unsigned long l = strtoul(a, &tail, 0);
     
    if (NULL == l) {
        printf("Not a number. \n");
    }

    printf("[%u] \n", l);

    return 0;
}
