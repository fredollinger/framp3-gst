#ifndef FRAMLIB
#define FRAMLIB

// define file types
#define FRAM_ERR -1
#define FRAM_DIR 0
#define FRAM_NULL -2
#define FRAM_MP3 1
#define FRAM_OGG 2
#define FRAM_WAVE 3
#define FRAM_MPEG 4

int fram_cd(char*);
int fram_ls(void);
int get_song_name(int songnum, char *song);

#endif // FRAMLIB
