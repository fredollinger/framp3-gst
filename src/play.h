#ifndef PLAY
#define PLAY

// Given a path, play a song
void play(char*);
void stop(void);
int pause2(void);
void resume(void);
// Given a path, play a song in a thread
void *play_t(void *);
void framp3_init(int argc, char *argv[]);

#endif // PLAY
