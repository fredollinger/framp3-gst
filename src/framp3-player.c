#include <fcntl.h>
#include <pthread.h>
#include <readline/readline.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>

#include "netlab.h"
#include "play.h"

char* get_cmd(int readfd) {
    char buf[1000];
    char *cmd;
    size_t bytes = read(readfd, &buf, 999);
    cmd = (char *) malloc (bytes+1);
    strncpy(cmd, buf, bytes);
}

void player_process() {

    framp3_init (0, NULL);
    pthread_t thread;

    char *cmd, *song;
    const char *resp="fin";
    int writefd, readfd;
    printf("player opening fifo: [%s] \n", FRAMP3_COMMAND_SOCK);
    readfd=open(FRAMP3_COMMAND_SOCK, O_RDONLY);
    writefd=open(FRAMP3_RESPONSE_SOCK, O_WRONLY, 0);

    while (1) {
	printf("running...\n");
        cmd = get_cmd(readfd);

	// If we have a number play that song
        char *tail;
        unsigned long songnum = strtoul(cmd, &tail, 0);
        if (NULL != songnum) {
	    printf("playing song STUB [%s] \n", get_song_name(songnum));
	    continue;
	}

        if (0 == strncmp(cmd, "quit", 4)){
	    printf("framp3-player quitting...\n");
            exit(0);
        }
        else if (0 == strncmp(cmd, "play ", 5)){
            song = (char *) malloc (strlen(cmd)-5);
            strncpy(song, cmd + 5, strlen(cmd));
    	    free(cmd);
            printf("playing %s \n", song);
            pthread_create(&thread, NULL, play_t, (void*) song);
        }
        else if (0 == strncmp(cmd, "pause", 5)){
            printf("pausing");
	    pause();
	}
        else if (0 == strncmp(cmd, "ls", 6)){
            printf("ls");
	    fram_ls();
	}
        else if (0 == strncmp(cmd, "resume", 6)){
            printf("resume");
	    resume();
	}
        else if (0 == strncmp(cmd, "stop", 4)){
	    stop();
	}
        else {
            printf("unknown cmd\n");
        }
        sleep(1);
    }

    unlink(FRAMP3_COMMAND_SOCK);
    unlink(FRAMP3_RESPONSE_SOCK);
} // END player_process()
