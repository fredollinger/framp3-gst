#include "config.h"
#include "play.h"

#include <nih/main.h>
#include <nih/option.h>

#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void usage(){
    printf("framp3-gst filename \n\n");
    printf("Options: \n\n");
    printf("-p playlist of songs to play \n\n");
}

static int play_list (NihOption *option, const char *arg)
{
    printf("%s [%s] \n", __PRETTY_FUNCTION__, arg);
    FILE *infile;
    char song[BUFSIZ];
    infile = fopen(arg, "r");
    if (!infile) {
        printf("Couldn't open file %s for reading.\n", arg);
	return 0;
    }
    while (fgets(song, sizeof(song), infile)) {
	play(strtok(song, "\n"));
    }
    fclose(infile);
    exit(0);
}

static NihOption options[] = {
    { 'p', "playlist", N_("file with a list of songs to play"),
        NULL, "songs to play", NULL, play_list },
    NIH_OPTION_LAST
};

int main(int argc, char *argv[]) {
    char ** args;
    if ( argc < 2 ) {
      usage();
      return(-1);
    }

    framp3_init (argc, argv);

    // BEGIN ARGS
    // Deal with command line arguments
    nih_main_init (argv[0]);
    nih_option_set_synopsis (_("framp3 command line music player"));
    nih_option_set_help (_("framp3 command line music player"));
    args = nih_option_parser (NULL, argc, argv, options, FALSE);
    // END ARGS

    int i;
    for ( i = 1; i < argc; i++ ) {
      printf("%s \n", argv[i]);
      play(argv[i]);
    } // END for()

    return 0;
}
