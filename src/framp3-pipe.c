#include <fcntl.h>
#include <pthread.h>
#include <readline/readline.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>

#include "framp3-player.h"
#include "framlib.h"
#include "netlab.h"
#include "play.h"

void parent() {
    char *send;
    char buf[1000];
    int writefd, readfd;
    pid_t pid;

    int res = mkfifo(FRAMP3_COMMAND_SOCK, FILE_MODE);
    res = mkfifo(FRAMP3_RESPONSE_SOCK, FILE_MODE);
    //printf("making fifo: [%s] result: [%i] \n", FRAMP3_COMMAND_SOCK, res);

    pid = fork();

    if ( 0 == pid ) {
        player_process();
	exit(0);
    }

    writefd=open(FRAMP3_COMMAND_SOCK, O_WRONLY, 0);
    readfd=open(FRAMP3_RESPONSE_SOCK, O_RDONLY, 0);
    while(1) {
        send = readline ("\n mp3> ");
        if ( 0 == strncmp(send, "cd", 2)){
	    printf("cd %s \n", send);
            fram_cd(send);
        }
        else if ( 0 == strncmp(send, "ls", 2)){
            fram_ls();
        }
        else if ( 0 == strncmp(send, "quit", 4)){
            write(writefd, send,  strlen(send));
	    exit(0);
	}
        else {
            write(writefd, send,  strlen(send));
	}
    } // END while(1)
}

int main(int argc, char *argv[]){
    parent();
}
