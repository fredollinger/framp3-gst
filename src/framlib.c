/* framlib.c
 *
 *   This file is part of framplib.c
 *
 *   Copyright 2016 Frederick Ollinger <follinge@gmail.com>
 *
 *   Non-threaded, utility functions (non-playing)
 *
 */

#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#include "framlib.h"

int ismp3(char *s)
{
    FILE *fi;
    int c;
    fi = fopen(s, "r");

    if ( 0 != errno) return FRAM_ERR;

    c = fgetc (fi);
    if (73 != c)
    {
        fclose(fi);
        return FRAM_ERR;
    }
    c = fgetc (fi);
    if (68 != c)
    {
        fclose(fi);
        return FRAM_ERR;
    }
    c = fgetc (fi);
    if (51 != c)
    {
        fclose(fi);
        return FRAM_ERR;
    }
    fclose(fi);
    return FRAM_MP3;
}

int fram_ls()
{
    DIR *dir;
    struct dirent *dp;
    dir = opendir(".");

    FILE *pager;
    char *cpager="less";
    pager = popen (cpager, "w");

    if (!pager)
    {
        fprintf (stderr,
            "incorrect parameters or too many files.\n");
        //return EXIT_FAILURE;
        return -1;
    }

    if (dir == NULL)
    {
        printf ("\nopendir error\n");
        return -1;
    }

    dp = readdir (dir);
    int counter = 1;

    while (dp != NULL)
    {
        char *s;
        char *copys;
        int i=0;
        int ans=0;

        copys=dp->d_name;
        //ans = issong(copys);
        ans = ismp3(copys);

//        if (ans > 0)
//        {
            //printf("%i: %s\n", counter, copys);
            fprintf(pager, "%i: %s\n", counter, copys);
            printf("%i: %s\n", counter, copys);
            counter ++;
//        }

/*
        if (ans == FRAM_DIR)
        {
            fprintf(pager, "dir: %s\n", copys);
            printf("dir: %s\n", copys);
        }
*/

        dp = readdir(dir);
    } // END while()

    closedir (dir);
    pclose (pager);
    return 0;
}

int fram_cd(char *str) {
    int err;
    const char s[2] = " ";
    char *token;

    token = strtok(str, s);
    if (NULL == token) return -1;
    token = strtok(NULL, s);
    printf( "cd [%s]\n", token );
    err = chdir(token);

    return err;
} // END fram_cd()

// return the song of the number given
int get_song_name(int songnum, char *song)
{
    int songtype;
    int counter = 0;
    int cnt = 0;
    struct dirent **eps;
    int n;

    //n = scandir ("./", &eps, fram_one, alphasort);
    n = scandir ("./", &eps, 0, alphasort);

    if (n < 0)
    {
        printf("No songs available.\n");
        //printf("freeing eps! \n");
        free(eps);
        return 0;
    }

    while(cnt < n)
        {
        char *s;
            static char *copys;
            int i=0;

        copys=eps[cnt]->d_name;
        songtype = ismp3(copys);

            if (songtype > 0)
            {
                    counter ++;
            }
        else
        {
                //printf ("get_song_name(): is NOT song %s\n", copys);
        }

        if (counter == songnum)
        {
            strcpy(song, copys);
            //printf("freeing eps! \n");
            free(eps);
            //printf("freeing copys! \n");
            //free(copys);
            return 0;
        }

        cnt++;
    }

    //printf("freeing eps! \n");
    free(eps);
    return -1;
}
