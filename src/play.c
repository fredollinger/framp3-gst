#include <gst/gst.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#include "play.h"

static const int char_max = 1000;
static GstElement *pipeline;
static GstBus *bus;
static char *lpath;
static int running = FALSE;

void framp3_init(int argc, char *argv[]) {
    gst_init (&argc, &argv);
}

// If we stop for any reason call this to not leak memory
void cleanup () {

    if (bus) {
        gst_object_unref (bus);
        bus = 0;
    }
    if (pipeline) {
        gst_element_set_state (pipeline, GST_STATE_NULL);
        gst_object_unref (pipeline);
        pipeline = 0;
    }
    if (lpath) {
        free(lpath);
	lpath = 0;
    }
    running = FALSE;
}

void *play_t(void *voidoid) {
    char *song = (char*) voidoid;
    printf("play [%s] \n", song);
    play(song);
}

int pause2() {
    printf("pausing \n");
    int ret = gst_element_set_state (pipeline, GST_STATE_PAUSED);
    if (ret == GST_STATE_CHANGE_FAILURE) {
         g_printerr ("Unable to set the pipeline to the paused state.\n");
	 return -1;
    }
    return 0;
}

void resume() {
    printf("resuming \n");
    int ret = gst_element_set_state (pipeline, GST_STATE_PLAYING);
    if (ret == GST_STATE_CHANGE_FAILURE) {
         g_printerr ("Unable to set the pipeline to the playing state.\n");
    }
}

void stop() {
    if (!running) {
        printf("%s not running! \n", __PRETTY_FUNCTION__);
	return;
    }
    pause2();
    cleanup();
}

void play(char *path) {
   if (running) {
        printf("%s all ready running! \n", __PRETTY_FUNCTION__);
	return;
    }

    lpath = path;
    bus = NULL;
    printf("playing [%s] \n", path);

    GstMessage *msg = NULL;
    GstStateChangeReturn ret;
    GError *err=NULL;
    char cmd[char_max];

    // gst-launch-1.0 -v filesrc location=hello.mp3 ! mad ! alsasink
    snprintf(cmd, char_max, "filesrc location=\"%s\" ! mad ! alsasink", path);

    pipeline = gst_parse_launch (cmd, &err);

    if( err != NULL )
    {
      printf("%i \n", err->code);
      printf("%s \n", err->message);
      g_clear_error (&err);
     }

     /* Start playing */
     printf("playing...\n");
     ret = gst_element_set_state (pipeline, GST_STATE_PLAYING);
     if (ret == GST_STATE_CHANGE_FAILURE) {
         g_printerr ("Unable to set the pipeline to the playing state.\n");
         gst_object_unref (pipeline);
         return;
     }

     /* Wait until error or EOS */
     bus = gst_element_get_bus (pipeline);
     printf("crashing...\n");
     if (NULL == bus ) {
         printf("failed to get bus...\n");
	 exit(0);
     }
     running = TRUE;
     //msg = gst_bus_timed_pop_filtered (bus, GST_CLOCK_TIME_NONE, GST_MESSAGE_ERROR | GST_MESSAGE_EOS);
     //running = FALSE;

#if 0
    /* Parse message */
    if (msg != NULL) {
      GError *err;
      gchar *debug_info;

      switch (GST_MESSAGE_TYPE (msg)) {
        case GST_MESSAGE_ERROR:
          gst_message_parse_error (msg, &err, &debug_info);
          g_printerr ("Error received from element %s: %s\n", GST_OBJECT_NAME (msg->src), err->message);
          g_printerr ("Debugging information: %s\n", debug_info ? debug_info : "none");
          g_clear_error (&err);
          g_free (debug_info);
          break;
        case GST_MESSAGE_EOS:
          g_print ("End-Of-Stream reached.\n");
          break;
        default:
          /* We should not reach here because we only asked for ERRORs and EOS */
          g_printerr ("Unexpected message received.\n");
          break;
      }
      gst_message_unref (msg);
    }

    cleanup();
#endif

/*
    gst_object_unref (bus);
    gst_element_set_state (pipeline, GST_STATE_NULL);
    gst_object_unref (pipeline);
    free(path);
*/
}
