Concepts
--------

These are what make gstreamer go.

Elements
========

Inherit from gobject, these are the things you piece together to make a pipeline.

Pipeline
========

Loosely modeled after a command line pipeline, this is where the media is piped through so it can be modified in various ways.

TODO: GIVE EXAMPLES OF SIMPLE PIPEPLINES.
