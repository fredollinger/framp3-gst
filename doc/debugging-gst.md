Introduction 
------------

Tutorial on how to debug gstreamer code.

Command line tools
------------------

gst-inspect
===========

gst-inspect allows one to test for whether a plug in is installed.

 $ gst-inspect videotestsrc

 No such element or plugin 'videotestsrc'

We can see that the plugin videotestsrc

